package main

import (
	"encoding/hex"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"

	"github.com/codegangsta/negroni"
	"github.com/convox/sparc/githubauth"
)

var port string = "5000"

func keys() []*[32]byte {
	// e.g. faba0c08be7474a785b272c4f4154c998c0943b51e662637be11b1a0ecda43b3
	key, err := hex.DecodeString(os.Getenv("KEY"))
	if err != nil {
		log.Fatal("Invalid key %v: %v\n", key, err)
	}
	if len(key) != 32 {
		log.Fatal("%v wasn't 32 bytes\n")
	}

	var key_array [32]byte
	copy(key_array[:], key)
	return []*[32]byte{&key_array}
}

func main() {
	if p := os.Getenv("PORT"); p != "" {
		port = p
	}

	mux := http.NewServeMux()

	mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
	})

	mux.HandleFunc("/auth", func(w http.ResponseWriter, req *http.Request) {
		u, err := url.Parse("https://github.com/login/oauth/authorize")

		if err != nil {
			panic(err)
		}

		q := u.Query()
		q.Set("client_id", os.Getenv("GITHUB_CLIENT_ID"))
		q.Set("redirect_uri", "http://192.168.59.103:5000/auth/callback")
		u.RawQuery = q.Encode()

		http.Redirect(w, req, u.String(), 302)
	})

	mux.HandleFunc("/auth/callback", func(w http.ResponseWriter, req *http.Request) {
	})

	mux.Handle("/auth/github", &githubauth.Handler{
		Keys:         keys(),
		ClientID:     os.Getenv("GITHUB_CLIENT_ID"),
		ClientSecret: os.Getenv("GITHUB_CLIENT_SECRET"),
	})

	n := negroni.Classic()
	n.UseHandler(mux)
	n.Run(fmt.Sprintf(":%s", port))
}
