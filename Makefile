PORT ?= 3000
PROJECT_NAME ?= sparc

.PHONY: default dev

all: build

build:
	go get ./...

dev:
	@forego run docker-compose up

vendor:
	godep save -r -copy=true ./...

docker-clean:
	docker-compose stop ;\
	docker-compose rm --force ;\
	docker rmi -f `docker images | grep $(PROJECT_NAME) | tr -s ' ' | cut -d ' ' -f 3`
