# SPARC

Week over week metrics dashboard for:

Security        - Library Age
Performance     - Request Time Perc95
Availability    - Request 500s, Ping
Rate of Change  - GitHub Commits, AWS Deploys
Cost            - AWS Spend

## Rate of Change

### GitHub OAuth Notes

https://github.com/organizations/convox/settings/applications/194751
  http://192.168.59.103:5000/_githubauth
https://github.com/settings/applications

https://developer.github.com/v3/oauth/#web-application-flow
https://developer.github.com/v3/repos/#list-organization-repositories
https://developer.github.com/v3/repos/statistics/#commit-activity
https://developer.github.com/v3/repos/statistics/#code-frequency

* GitHub OAuth Web Application Flow
* GitHub Stats API (retry on 202)
* Log as measurement
* PUT to CloudWatch API

* Expose as API?
* Render as week-over-week graph
* Refresh?
* Data binding?

* Cloudwatch - http://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/publishingMetrics.html
* Elasticsearch, Logstash, Kibana (ELK) stack?
* Librato?
* https://www.scalyr.com/
* Splunk?
* Datadog?

## Journal

25 minute sessions

1 - R&D, create GitHub and AWS credentials
2 - Go GitHub OAuth Web Flow
  JAWS, Boot2Docker, make dev is nice
  http://192.168.59.103:5000/

  Annoying problems...
    Browser is caching /auth redirect, doesn't load new code. /auth1 ... /auth5
    Can't figure out URL parsing

3 - Research stdlib OAuth2 options
  https://github.com/kr/githubauth/blob/master/githubauth.go
  session keys??
  
4 - *[32]byte
  pointer to [32]byte type
  compiler succeeding, but immediate panic
  [rerun] build succeeded
  web_1 | [rerun] install succeeded
  web_1 | panic: runtime error: index out of range
  web_1 | 
  web_1 | goroutine 16 [running]:
  web_1 | runtime.panic(0x69f040, 0x85977c)
  web_1 |   /usr/lib/go/src/pkg/runtime/panic.c:279 +0xf5
  web_1 | main.main()
  web_1 |   /go/src/github.com/convox/jaws/web.go:24 +0x516
  web_1 | 
  web_1 | goroutine 17 [runnable]:
  web_1 | runtime.MHeap_Scavenger()
  web_1 |   /usr/lib/go/src/pkg/runtime/mheap.c:507
  web_1 | runtime.goexit()
  web_1 |   /usr/lib/go/src/pkg/runtime/proc.c:1445
  web_1 | 
  web_1 | goroutine 18 [runnable]:
  web_1 | bgsweep()
  web_1 |   /usr/lib/go/src/pkg/runtime/mgc0.c:1976
  web_1 | runtime.goexit()
  web_1 |   /usr/lib/go/src/pkg/runtime/proc.c:1445
  web_1 | 
  web_1 | goroutine 19 [runnable]:
  web_1 | runfinq()
  web_1 |   /usr/lib/go/src/pkg/runtime/mgc0.c:2606
  web_1 | runtime.goexit()
  web_1 |   /usr/lib/go/src/pkg/runtime/proc.c:1445

  Try IRC...

  http://play.golang.org/p/pDGtw1dNRV

5 - more Debugging githubauth
  godep?
  godep: directory "/Users/noah/dev/sparc" is outside source root "src"

  go build -a && forego run ./sparc

  discovered https://github.com/freeformz/googlegoauth/commit/c95d4cef1ba774ff4da23499b46cfa34588c6c77
  kr, liz and ed...

  vendor in
  change http vs https

6 - get access token
  Why doesn't library do this?!
  POST https://github.com/login/oauth/access_token


## License

Copyright Noah Zoschke 2015
